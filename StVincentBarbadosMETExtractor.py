import json

from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


# St.VincentCurrentForecastExtractor
class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "https://www.barbadosweather.org/getSVGWxForecastDataResp.php",
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {"type": "text", "unit": "km/h"},
            "seas": {"type": "text", "unit": "m"},
        }

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "Synopsis"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        caribbean_wrap = BeautifulSoup(
            str(soup.select(".caribbeanWrap")[0]), "html.parser"
        )
        titles = caribbean_wrap.select(".sideBarBoxTitle2")
        today_index = -1
        tonight_index = -1

        for index, title in enumerate(titles):
            title_text = self.clean_text(title.text.lower())
            if "today" in title_text:
                today_index = index
                self.readings["Forecasted Period (today)"] = self.clean_text(
                    title_text.split("today")[1]
                )
            elif "tonight" in title_text:
                tonight_index = index
                self.readings["Forecasted Period (tonight)"] = self.clean_text(
                    title_text.split("tonight")[1]
                )

        forecasts = []
        synopses = []
        winds = []
        seas = []

        div_sections = caribbean_wrap.find_all("div")[1:]
        for sec in div_sections:
            div_content = self.clean_text(sec.text)
            if len(div_content) > 0:
                if "Forecast:" in div_content:
                    forecasts.append(div_content)
                elif "Synopsis:" in div_content:
                    synopses.append(div_content)
                elif "Wind:" in div_content:
                    winds.append(div_content)
                elif "Seas:" in div_content:
                    seas.append(div_content)

        print(
            "forecast: {0}, synposis: {1}, wind: {2}, sea: {3}".format(
                len(forecasts),
                len(synopses),
                len(winds),
                len(seas),
            )
        )

        self.readings["Synopsis"] = "Today is {0} Tonight {1}".format(
            self._process_reading(synopses[today_index], "Synopsis:"),
            self._process_reading(synopses[tonight_index], "Synopsis:")
        )
        self.readings["Winds"] = "Today is {0} Tonight will be {1}".format(
            self._process_reading(winds[today_index], "Wind:"),
            self._process_reading(winds[tonight_index], "Wind:")
        )
        self.readings["Forecasts"] = "Today is {0} Tonight will be {1}".format(
            self._process_reading(forecasts[today_index], "Forecast:"),
            self._process_reading(forecasts[tonight_index], "Forecast:")
        )
        self.readings["Seas"] = "Today is {0} Tonight will be {1}".format(
            self._process_reading(seas[today_index], "Seas:"),
            self._process_reading(seas[tonight_index], "Seas:")
        )

        return self.readings

    def _process_reading(self, div_content, reading_type):
        try:
            return self.clean_text(div_content.split(reading_type)[1])
        except:
            return self.clean_text(div_content)

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "Forecasted Period (today)",
            "Forecasted Period (tonight)",
            "Forecasts",
            "Synopsis",
            "Winds",
            "Seas",
            "source",
            "date"
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
