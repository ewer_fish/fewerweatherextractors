import json


try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self, "http://www.weather.gov.dm/forecast")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {"type": "text", "unit": "km/h"},
            "seas": {"type": "text", "unit": "m"},
        }

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "outlook"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        try:
            outlook = soup.find_all("div", {"class": "outlook_da_la col-sm-6"})
            self.readings["outlook"] = self.clean_text(outlook[0].text.strip())
        except Exception as e:
            print("Error while attempting to retrieve outlook {0}".format(e))

        items = []
        for div in soup.find_all("div", {"itemprop": "articleBody"}):
            for d in div.find_all("div"):
                for p in d.find_all("p"):
                    items.append(self.clean_text(p.text))

        for i in items:
            print(i)
            try:
                self.readings["synopsis"] = self.clean_text(
                    self.findword("synopsis", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve synopsis {0}".format(e))

            try:
                self.readings["wind"] = self.clean_text(
                    self.findword("wind", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve wind {0}".format(e))

            try:
                self.readings["waves"] = self.clean_text(
                    self.findword("waves", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve waves {0}".format(e))

            try:
                self.readings["sea conditions"] = self.clean_text(
                    self.findword("sea conditions", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error retrieving sea conditions {0}.".format(e))
                try:
                    self.readings["seas"] = self.clean_text(
                        self.findword("sea", items)[0].split(": ")[1]
                    )
                except Exception as e:
                    print("Error while attempting to retrieve sea {0}".format(e))

            try:
                self.readings["advisory"] = self.clean_text(
                    self.findword("advisory", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve sea conditions {0}".format(e))

            try:
                self.readings["sunrise"] = self.clean_text(
                    self.findword("sunrise", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve sunrise {0}".format(e))

            try:
                self.readings["sunset"] = self.clean_text(
                    self.findword("sunset", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve sunset {0}".format(e))

            try:
                self.readings["low tide"] = self.clean_text(
                    self.findword("low tide", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve low tide {0}".format(e))

            try:
                self.readings["high tide"] = self.clean_text(
                    self.findword("high tide", items)[0].split(": ")[1]
                )
            except Exception as e:
                print("Error while attempting to retrieve low tide {0}".format(e))

            try:
                self.readings["forecast"] = self.clean_text(
                    self.findword("forecast", items)[0].strip()
                )
            except Exception as e:
                print("Error while attempting to retrieve forecast {0}".format(e))

            try:
                self.readings["date"] = self.clean_text(
                    self.findword("valid", items)[0].strip()
                )
            except Exception as e:
                print("Error while attempting to retrieve date {0}".format(e))

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "date",
            "advisory",
            "outlook",
            "forecast",
            "seas",
            "sea conditions",
            "waves",
            "synopsis",
            "wind",
            "low tide",
            "high tide",
            "sunrise",
            "sunset",
            "source",
            "order",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
    order = set(country.defineOrder())
    received_keys = set(country.readings.keys())
    print(order.difference(received_keys))
