import json
import logging

log = logging.getLogger(__name__)

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://meteo.gov.vc/meteo/index.php/latest-forecast/"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {"type": "numerical", "unit": "km/h"},
            "seas": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "Forecast"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        headings = []
        details = []
        count = 1
        index = 0

        # Current weather on right side
        data = soup.find_all("li")

        try:
            title = self.clean_text(soup.select(".catItemTitle")[0].text)
            self.readings["Date"] = title.split("-")[1]
        except Exception as e:
            log.error("Unable to get data: {0}".format(e))

        for row in data:
            row_text = row.text.encode("ascii", "ignore").decode("ascii").lower()

            if "temperature" in row_text:
                self.readings["Temperature"] = self.clean_text(row_text.split(":")[1])
            if "conditions" in row_text:
                self.readings["Summary"] = self.clean_text(row_text.split(":")[1])
            if "humidity" in row_text:
                self.readings["Relative Humidity"] = self.clean_text(
                    row_text.split(":")[1]
                )
            if "pressure" in row_text:
                self.readings["Barometric Pressure"] = self.clean_text(
                    row_text.split(":")[1]
                )
            if "sunrise" in row_text:
                comps = [self.clean_text(txt) for txt in row_text.split(":")]
                self.readings["Sunrise"] = "{0}:{1}".format(comps[1], comps[2])
            if "sunset" in row_text:
                comps = [self.clean_text(txt) for txt in row_text.split(":")]
                self.readings["Sunset"] = "{0}:{1}".format(comps[1], comps[2])
            if "rainfall" in row_text:
                self.readings["24 hour Rainfall"] = self.clean_text(
                    row_text.split(":")[1]
                )

        # Table latest forecast data
        data = soup.find_all("td")
        for row in data:
            if count % 2 == 1:
                headings.append(self.clean_text(row.text.strip()))
                count = count + 1
            else:
                details.append(self.clean_text(row.text.strip()))
                count = count + 1

        # Tides formatted different
        for heading in headings:
            if "tides" in heading.lower():
                break
            key = heading.replace(":", "")
            if key == "Weather Advisory/ Warning":
                key = "Weather Advisory/Warning"
            if key == "Marine Advisory/ Warning":
                key = "Marine Advisory/Warning"
            self.readings[key] = details[index]
            index += 1

        # getting tides separate
        tides = []
        table_rows = soup.find_all("tr")
        for row in table_rows:
            if "tides" in row.text.lower():
                tides = row.text.rstrip().split("\n")
                break

        tides = [
            self.clean_text(rec) for rec in tides if len(rec) > 1 and "**" not in rec
        ]

        tide_data = ""
        # Splitting by the High and Low to seperate them then concatenate them as a string removing the excess * on site
        try:
            index = tides.index("High:")
            tide_data += "High: " + self.clean_text(tides[index + 1])
        except Exception as e:
            log.error("Unable to get high tide: {0}".format(e))
        try:
            index = tides.index("Low:")
            tide_data += " Low: " + self.clean_text(tides[index + 1])
        except Exception as e:
            log.error("Unable to get low tide: {0}".format(e))

        # set the string to Tides
        self.readings["Tides"] = tide_data

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "Synopsis",
            "Date",
            "Forecast Period",
            "Forecast",
            "Weather Advisory/Warning",
            "Marine Advisory/Warning",
            "Sea Conditions",
            "Tides",
            "Temperature",
            "Winds",
            "Barometric Pressure",
            "Relative Humidity",
            "24 hour Rainfall",
            "Sunrise",
            "Sunset",
            "source",
        ]


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.StVincent
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
