import json

import requests

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self)
        self.date_range = ["20171201", "20171215"]
        self.station = "TEC4771"
        self.get_extractor_url()

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        self.extractor_url = "https://tidesandcurrents.noaa.gov/api/datagetter?product=predictions&application=NOS.COOPS.TAC.WL&begin_date={0}&end_date={1}&datum=MLLW&station={2}&time_zone=lst_ldt&units=english&interval=hilo&format=json".format(
            self.date_range[0], self.date_range[1], self.station  # begin date, end date
        )
        return self.extractor_url

    def get_reading_types(self):
        return {
            "High Tide": {
                "type": "numerical",
                "unit": "ft",
            },  # TODO Need a cross platform way to represent degree symbol
            "Low Tide": {"type": "numerical", "unit": "ft"},
        }

    def extract(self):
        self.readings["type"] = []
        self.readings["date"] = []
        self.readings["tide"] = []
        r = requests.get(self.extractor_url)
        for sensor in r.json()["predictions"]:
            self.readings["type"].append(sensor["type"])
            self.readings["date"].append(sensor["t"])
            self.readings["tide"].append(sensor["v"])

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
