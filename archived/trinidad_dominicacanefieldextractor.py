# -*- coding: utf-8 -*-
import json

import requests
from bs4 import BeautifulSoup

# If we running directly via command line
if __name__ == "__main__":
    try:
        # We are attempting to run via cli within mFisheries application
        from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
            WeatherSourceExtractor,
        )
    except:
        # We are attempting to run via cli outside of application
        from WeatherSourceExtractor import WeatherSourceExtractor
else:
    # The system is calling the package therefore we are within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://www.weather.gov.dm/current-conditions"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {
                "type": "numerical",
                "unit": "C",
            },  # TODO Need a cross platform way to represent degree symbol
            "windspeed": {"type": "numerical", "unit": "km/h"},
            "pressure": {"type": "numerical", "unit": "mb"},
            "humidity": {"type": "numerical", "unit": "%"},
        }

    def extract(self):
        self.readings = {}
        r = requests.get("http://www.weather.gov.dm/current-conditions")
        soup = BeautifulSoup(r.content, "html.parser")
        temp = []
        for row in soup.find_all("span", {"class": "curr_temp"}):
            temp.append(row.text.split("oC")[0])
        data = []
        table = soup.find_all("div", attrs={"class": "curr_cond_cf_no_bg"})
        for x in table:
            for a in x.find_all("p"):
                data.append(json.loads(json.dumps(a.text.split(":")[1])))

        winds = {}
        country = "Domonica - Canefield Airport"
        temperature = temp[0]
        humidity = data[1]
        pressure = data[2]
        winds = data[0]
        self.readings = {
            "temperature": temperature,
            "humidity": humidity,
            "pressure": pressure,
            "wind": winds,
        }

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
