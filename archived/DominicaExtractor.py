import json

import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://www.weather.gov.dm/current-conditions"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "Douglas-Charles Temperature": {"type": "numerical", "unit": "C"},
            "Canefield Temperature": {"type": "numerical", "unit": "C"},
            "Douglas-Charles Wind Speed": {"type": "numerical", "unit": "km/h"},
            "Canefield Wind Speed": {"type": "numerical", "unit": "km/h"},
            "Douglas-Charles Pressure": {"type": "numerical", "unit": "mb"},
            "Canefield Pressure": {"type": "numerical", "unit": "mb"},
            "Douglas-Charles Humidity": {"type": "numerical", "unit": "%"},
            "Canefield Humidity": {"type": "numerical", "unit": "%"},
        }

    def extract(self):
        self.readings = {}

        # Process the information from the current url
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content, "html.parser")

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, "r") as fp:
        # 	# fp.write(str(soup))
        # 	content = fp.read()
        # 	fp.close()
        # soup = BeautifulSoup(content, "html.parser")

        # Extracting information for Current Readings
        temp = []
        for row in soup.find_all("span", {"class": "curr_temp"}):
            temp.append(row.text.split("oC")[0])
        # Read Temperatures
        try:
            self.readings["Douglas-Charles Temperature"] = temp[1]
            self.readings["Canefield Temperature"] = temp[0]
        except Exception, e:
            print("Error while attempting to retrieve temperature {0}".format(e))

        # Read Summary
        temp = []
        for row in soup.find_all("p", {"class": "pres_weather"}):
            temp.append(row.text)
        try:
            self.readings["Douglas-Charles Summary"] = temp[1]
            self.readings["Canefield Summary"] = temp[0]
        except Exception, e:
            print("Error while attempting to retrieve summary {0}".format(e))

        # Process remaining fields
        data = []
        table = soup.find_all("div", attrs={"class": "curr_cond_cf_no_bg"})
        for x in table:
            for a in x.find_all("p"):
                data.append(json.loads(json.dumps(a.text.split(":")[1])))

        # Reading Humidity
        try:
            self.readings["Douglas-Charles Humidity"] = data[5].split("%")[0].strip()
            self.readings["Canefield Humidity"] = data[1].split("%")[0].strip()
        except Exception, e:
            print("Error while attempting to retrieve humidity {0}".format(e))

        # Reading Pressure
        try:
            self.readings["Douglas-Charles Pressure"] = data[6].split("mb")[0].strip()
            self.readings["Canefield Pressure"] = data[2].split("mb")[0].strip()
        except Exception, e:
            print("Error while attempting to retrieve pressure {0}".format(e))

        # Reading Wind
        try:
            dc_wind = data[4].strip().split(" /")
            cf_wind = data[0].strip().split(" /")

            self.readings["Douglas-Charles Wind Direction"] = dc_wind[0]
            self.readings["Canefield Wind Direction"] = cf_wind[0]

            self.readings["Douglas-Charles Wind Speed"] = dc_wind[1].split("km")[0]
            self.readings["Canefield Wind Speed"] = cf_wind[1].split("km")[0]

        except Exception, e:
            print("Error while attempting to retrieve winds {0}".format(e))

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
