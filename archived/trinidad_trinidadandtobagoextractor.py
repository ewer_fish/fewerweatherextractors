import json

import requests
from bs4 import BeautifulSoup

# If we running directly via command line
if __name__ == "__main__":
    try:
        # We are attempting to run via cli within mFisheries application
        from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
            WeatherSourceExtractor,
        )
    except:
        # We are attempting to run via cli outside of application
        from WeatherSourceExtractor import WeatherSourceExtractor
else:
    # The system is calling the package therefore we are within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )


def wordsize(stuff, word):
    string = ""
    pos = stuff.find(word) + len(word)
    while not (stuff[pos].isupper() and stuff[pos - 1].islower()):
        string += stuff[pos]
        pos += 1
    return string


def findword(word, array):
    for data in array:
        if word in data.lower():
            return data


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://metservice.gov.tt/forecast_api2.php"
        )
        self.debug = False

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"temperature": {"type": "numerical", "unit": "C"}}

    def extract(self):
        headings = []
        details = []
        self.readings = {}
        count = 0
        if self.debug:
            print("Extracting current info from Trinidad and Tobago MET\n")
        r = requests.get(self.extractor_url)
        if self.debug:
            print("Retrieved home page\n")
        soup = BeautifulSoup(r.content, "html.parser")
        stuff = filter(None, soup.text.split("\n"))
        temp = filter(None, soup.text.split("\n"))
        # print temp
        data = " ".join(stuff)
        self.readings["waves"] = wordsize(data, "Waves: ")
        self.readings["seas"] = wordsize(data, "Seas: ")
        sun = findword("sunset", stuff).split(" ")
        count = 0
        # print sun

        # print len(temp)

        for t in temp:
            # print t.lower()+"\n"
            if "tonight:" in t.lower():
                r = t.split("room")
                if self.debug:
                    print r[1] + "\n"
                self.readings["synopsis"] = r[1].replace("\r", "") + temp[
                    count + 1
                ].replace("\r", "")

            if "max. temp." in t.lower():
                if self.debug:
                    print t + " Piarco: " + temp[count + 1] + " Crown Point: " + temp[
                        count + 2
                    ] + "\n"
                self.readings["Forecasted Max Temp, Piarco"] = temp[count + 1]
                self.readings["Forecasted Max Temp, Crown Point"] = temp[count + 1]

            count = count + 1

        count = 0
        for i in sun:

            # print i.lower()

            if "sunset:" in i.lower():
                if self.debug:
                    print "Sunset: " + sun[count + 1]
                self.readings["sunset"] = sun[count + 1]

            if "AM" in i or "am" in i:
                if self.debug:
                    print "Sunrise: " + sun[count].strip()
                self.readings["sunrise"] = sun[count - 1].strip()
            count = count + 1

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.fewer.weather.parsers.GrenadaCurrentExtractor

if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
