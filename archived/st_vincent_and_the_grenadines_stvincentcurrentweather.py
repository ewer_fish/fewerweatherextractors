import json

import requests
from bs4 import BeautifulSoup

try:
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except ImportError, e:
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "http://meteo.gov.vc/meteo/",
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {"type": "numerical", "unit": "F"},
            "wind": {"type": "numerical", "unit": "km/h"},
            "pressure": {"type": "numerical", "unit": "hPa"},
            "Relative Humidity": {"type": "numerical", "unit": "%"},
        }

    def extract(self):
        headings = []
        self.readings = {}
        print("Extracting current info from Saint Vincent MET")
        r = requests.get(self.extractor_url)
        print("Retrieved home page")
        soup = BeautifulSoup(r.content, "html.parser")
        data = soup.find_all("ul")
        save = ""
        for row in data:
            # takes the block of text with readings alone. the ul has other unrelated stuff we didnt need
            if "Temperature" in row.text:
                save = row.text
                break
        for current in save.strip().split("\n"):
            self.readings[
                current.replace(u"\xa0", u" ").split(": ")[0]
            ] = current.replace(u"\xa0", u" ").split(": ")[1]

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.fewer.weather.parsers.StVincent
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print country.toJSON()
