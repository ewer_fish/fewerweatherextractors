import json

from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self, "http://www.weather.gov.dm/forecast")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {
                "type": "numerical",
                "unit": "C",
            },  # TODO Need a cross platform way to represent degree symbol
            "wind": {"type": "text", "unit": "km/h"},
            "waves": {"type": "text", "unit": "ft"},
        }

    def extract(self):
        dates = []
        max_temp = []
        min_temp = []
        wind = []
        wind_speed = []
        seas = []
        waves = []
        min_waves = []
        condition = []
        # r = requests.get(self.extractor_url)
        # soup = BeautifulSoup(r.content,"html.parser")

        # Strategy for caching request while developing
        filename = "input.html"
        with open(filename, "r") as fp:
            # fp.write(str(soup))
            content = fp.read()
            fp.close()
        soup = BeautifulSoup(content, "html.parser")

        try:
            extended = soup.find_all("div", {"id": "ext_forecast"})
            for i in extended:
                for date in i.find_all("h3"):
                    dates.append(date.text.replace("\n", " "))
            data = soup.find_all("div", {"class": "entry"})
            for entry in data:
                # Read Maximum Temperature
                try:
                    val = entry.text.split("\n")[1].split(": ")[1].split("/")[0]
                    val = val.split(" ")[0]
                    max_temp.append(val)
                except Exception, e:
                    print(
                        "Error while attempting to retrieve max temperature {0}".format(
                            e
                        )
                    )

                # Read Minimum Temperature
                try:
                    val = entry.text.split("\n")[2].split(": ")[1].split("/")[0]
                    val = val.split(" ")[0]
                    min_temp.append(val)
                except Exception, e:
                    print(
                        "Error while attempting to retrieve min temperature {0}".format(
                            e
                        )
                    )

                # Read Wind
                try:
                    val = entry.text.split("\n")[5].split(": ")[1]
                    wind.append(val)
                    speed = val.split("@")[1].split("to")[0].strip()
                    wind_speed.append(speed)

                except Exception, e:
                    print("Error while attempting to retrieve wind {0}".format(e))

                # Read Sea conditions
                try:
                    seas.append(entry.text.split("\n")[6].split(": ")[1])
                except Exception, e:
                    print(
                        "Error while attempting to retrieve sea conditions {0}".format(
                            e
                        )
                    )

                # Read Waves
                try:
                    val = entry.text.split("\n")[7].split(": ")[1]
                    val.replace("/", " or")
                    waves.append(val)

                    comp = val.split("to")

                    min_waves.append(comp[1])
                except Exception, e:
                    print("Error while attempting to retrieve waves {0}".format(e))

                # Read General Condition
                try:
                    condition.append(entry.text.split("\n")[4].split(": ")[1])
                except Exception, e:
                    print(
                        "Error while attempting to retrieve weather condition {0}".format(
                            e
                        )
                    )

        except Exception, e:
            print("Error with overall data extraction")

        # At this point we have an array of forecast for each days

        self.readings = {
            "dates": dates,
            "wind": wind,
            "waves": waves,
            "sea": seas,
            "condtions": condition,
            "mintemp": min_temp,
            "maxtemp": max_temp,
            "wind_speed": wind_speed,
            "wave_height": min_waves,
            "type": "forecast",
        }

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    print(country.toJSON())
