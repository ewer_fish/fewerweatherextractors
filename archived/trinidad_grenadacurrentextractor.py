import json

import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


def findword(word, array):
    for data in array:
        if word in data.lower():
            return data


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self, "http://weather.mbiagrenada.com/")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            # "temperature": {
            # 	"type": "numerical",
            # 	"unit": "F"
            # },  # TODO Need a cross platform way to represent degree symbol
            "wind speed": {"type": "numerical", "unit": "mph"},
            "waves": {"type": "numerical", "unit": "feet"}
            # "pressure": {
            # 	"type": "numerical",
            # 	"unit": "hPa"
            # },
            # "rel. humidity": {
            # 	"type": "numerical",
            # 	"unit": "%"
            # },
            # "visibility": {
            # 	"type": "numerical",
            # 	"unit": "km"
            # }
        }

    def extract(self):
        headings = []
        self.readings = {}
        details = []
        count = 0

        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content.decode("utf-8", "ignore"), "html.parser")

        # Extract forecast data in the main table
        main_table = soup.select("#main table")
        if len(main_table) == 1:
            forecast_table = main_table[0]
            for row in forecast_table.find_all("tr"):
                columns = row.find_all("td")
                if len(columns) > 1:
                    # Extract the First Column
                    components = columns[0].findAll(text=True)
                    attribute = (
                        components[1]
                        if len(components) > 1 and len(components[1]) > 2
                        else components[0]
                    )
                    # Extract the Second Column
                    components = columns[1].findAll(text=True)
                    value = components[1] if len(components) > 1 else components[0]
                    # print("Column 1: {0} \nColumn 2: {1}".format(attribute.encode('utf-8'), value.encode('utf-8')))
                    headings.append(attribute.encode("utf-8"))
                    details.append(value.encode("utf-8"))

        # To provide a clear distinction with similar title information we will put a prefix based on data
        duplicate_columns = ["temperature", "wind"]

        # Extract current weather readings from side column
        current_weather_sec = soup.find_all("div", {"class": "weather"})
        for row in current_weather_sec:
            for list_item in row.find_all("li"):
                rec = list_item.text.split(": ")
                if len(rec) > 1:
                    # self.readings[rec[0].lower()] = rec[1]
                    temp_head = rec[0].lower().encode("utf-8")
                    temp_head = (
                        "Current {0}".format(temp_head)
                        if temp_head in duplicate_columns
                        else temp_head
                    )
                    headings.append(temp_head)
                    details.append(rec[1].encode("utf-8"))

        # Attempt to clean up values while assigning to readings
        for key in headings:
            col = key.lower().replace(":", "").replace("\xc2\xa0", "")
            value = details[count].replace("\xc2\xa0", " ").replace("\xe2\x80\x89", " ")
            self.readings[col] = value
            count += 1

        # extract the numerical wind speed from readings
        if "wind" in self.readings:
            wind_text_comp = self.readings["wind"].split("@")[1].split(" ")
            for comp in wind_text_comp:
                if len(comp.split("-")) > 1:
                    max_temp = comp.split("-")[1]
                    self.readings["wind speed"] = max_temp

        # extract the sea state from seas reading
        if "seas" in self.readings:
            sea_text_comp = self.readings["seas"].split(" ")
            for comp in sea_text_comp:
                if len(comp.split("ft")) > 1:
                    self.readings["waves"] = comp.split("ft")[0]

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.fewer.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    print(country.toJSON())
