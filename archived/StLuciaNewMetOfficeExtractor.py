import json

import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


def findword(word, array):
    for data in array:
        if word in data.lower():
            return data


def findwordList(word, array):
    save = ""
    for data in array:
        if word in data.lower():
            save = data
    return save


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self, "https://met.gov.lc/index.php?page=start")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {"type": "numerical", "unit": "C"},
            "wind speed": {"type": "numerical", "unit": "mph"},
            "waves": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        items = []
        forecast = []
        self.readings = {}

        # Make Request from the Website
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content.decode("utf-8", "ignore"), "html.parser")

        # Setting the source for the readings
        self.readings["source"] = self.extractor_url

        # Specify the key of the data to be converted to voice
        self.readings["voiceField"] = "synopsis"

        data = soup.find_all("p")

        bolds = []
        for row in data:
            bolds = row.find_all("b")

        try:  # extract sunrise and sunset
            sun = self.findword("sunrise", items)[0].split(" ")
            count = 1
            for i in sun:
                try:
                    if "AM" in i:
                        self.readings["sunrise"] = sun[count - 1].strip()
                    elif "PM" in i:
                        self.readings["sunset"] = sun[count - 1].strip()
                    count += 1
                except Exception as e:
                    print("Error processing sunset and sunrise: {0}".format(e))
        except Exception as e:
            print("Error processing sunset and sunrise: {0}".format(e))

        print(self.readings)

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        order = [
            "date",
            "present weather",
            "weather forecast",
            "temperature",
            "current wind",
            "wind speed (kmh)",
            "wind speed (mph)",
            "tides for castries harbour",
            "tides for vieux fort bay",
            "seas",
            "waves",
            "sunrise",
            "sunset",
            "source",
            "order",
        ]
        return order


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    # print(country.toJSON())
    # with open('output.json', "w") as fp:
    #     fp.write(country.toJSON())
    #     fp.close()
