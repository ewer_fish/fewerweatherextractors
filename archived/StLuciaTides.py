import json

import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "https://www.tideschart.com/Martinique/Martinique/Martinique/Vieux-Fort-Bay-(Saint-Lucia)/",
        )
        self.debug = False

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"waves": {"type": "numerical", "unit": "m"}}

    def extract(self):
        self.readings = {}
        # Process the information from the current url
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content, "html.parser")

        # Strategy for caching request while developing
        filename = "input.html"
        with open(filename, "w") as fp:
            fp.write(str(soup))
            # content = fp.read()
            fp.close()
        # soup = BeautifulSoup(content, "html.parser")

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
