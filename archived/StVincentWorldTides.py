import json

import requests

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.modules.fewer.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self)
        self.api_key = "59a668e2-1d3e-4b5e-aeb7-2b63a5df065d"
        self.location = [12.9843, -61.2872]
        self.extractor_url = (
            "https://www.worldtides.info/api?extremes&lat={0}&lon={1}&key={2}".format(
                self.location[0], self.location[1], self.api_key
            )
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {}

    def extract(self):
        self.readings = {}
        items = []
        forecast = []
        r = requests.get(self.extractor_url)
        self.readings["readings"] = r.json()["extremes"]
        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
