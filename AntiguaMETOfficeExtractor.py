import json
import re

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://www.antiguamet.com/Antigua_Met_files/Daily_FCast.html"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "winds": {"type": "numerical", "unit": "mph"},
            "seas": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "synopsis"}
        soup = self.make_http_request_and_parse()
        date = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        paragraphs = soup.find_all("p")
        table_rows = soup.find_all("tr")

        for row in table_rows:
            if "high" in row.text.lower():
                high = row.text.replace(" ", "")
                high = re.sub("[^0-9]", "", high.split("C")[0])
                self.readings["high Temp"] = "{0} C".format(high)

            if "low" in row.text.lower():
                low = row.text.replace(" ", "")
                low = re.sub("[^0-9]", "", low.split("C")[0])
                self.readings["low Temp"] = "{0} C".format(low)

        # The forecast time is usually the second to last row.
        temp_attempt = table_rows[-2].text
        # Ensure that we have the time that the forecast was made
        if "forecast" in temp_attempt.lower():
            date = temp_attempt
        # Set the date time either based on current reading time or when extracted from the website
        self.readings["date"] = date

        for x in paragraphs:
            try:
                if "synopsis: " in x.text.lower():
                    self.readings["synopsis"] = self.clean_text(x.text.split(":")[1])
            except Exception as e:
                print("Error while attempting to retrieve synopsis {0}".format(e))

            try:
                if "seas:" in x.text.lower():
                    self.readings["seas"] = self.clean_text(x.text.split(":")[1])
            except Exception as e:
                print("Error while attempting to retrieve seas {0}".format(e))

            try:
                if "winds:" in x.text.lower():
                    self.readings["winds"] = self.clean_text(x.text.split(":")[1])
            except Exception as e:
                print("Error while attempting to retrieve winds {0}".format(e))

            try:
                if "sunset" in x.text.lower():
                    temp_text = x.text.split(":")
                    self.readings["sunset"] = self.clean_text(
                        temp_text[1] + ":" + temp_text[2]
                    )
            except Exception as e:
                print("Error while attempting to retrieve sunset {0}".format(e))

            try:
                if "sunrise" in x.text.lower():
                    temp_text = x.text.split(":")
                    self.readings["sunrise"] = self.clean_text(
                        temp_text[1] + ":" + temp_text[2]
                    )
            except Exception as e:
                print("Error while attempting to retrieve sunrise {0}".format(e))

            try:
                if "pressure" in x.text.lower():
                    self.readings["pressure"] = self.clean_text(x.text)
            except Exception as e:
                print("Error while attempting to retrieve pressure {0}".format(e))

            try:
                if "weather tonight" in x.text.lower():
                    content = self.clean_text(x.text.lower())
                    reading = content.split("weather tonight:")[1].split(".")[0]
                    self.readings["tonight"] = reading.replace("      ", "")
            except Exception as e:
                print("Error while attempting to retrieve tonight {0}".format(e))

            try:
                if "weather today" in x.text.lower():
                    content = self.clean_text(x.text.lower())
                    reading = content.split("weather today:")[1].split(".")[0]
                    self.readings["today"] = reading.replace("      ", "")
            except Exception as e:
                print("Error while attempting to retrieve tonight {0}".format(e))

            try:
                if "weather tomorrow" in x.text.lower():
                    content = self.clean_text(x.text.lower())
                    reading = content.split("weather tomorrow:")[1].split(".")[0]
                    self.readings["tomorrow"] = reading.replace("      ", "")
            except Exception as e:
                print("Error while attempting to retrieve tonight {0}".format(e))

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "date",
            "high Temp",
            "low Temp",
            "synopsis",
            "today",
            "tonight",
            "seas",
            "winds",
            "sunset",
            "sunrise",
            "pressure",
            "source",
            "order",
        ]


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    print(country.get_reading_types())
    country.extract()
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
