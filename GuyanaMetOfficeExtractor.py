import json

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


# St.VincentCurrentForecastExtractor
class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self, "http://hydromet.gov.gy/")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"wind": {"type": "text", "unit": "km/h"}}

    def extract(self):
        self.readings = {"source": self.extractor_url}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        forecast = []

        # Fetch 12Hr forecast in table
        forecasts = soup.find_all("td")
        paragraphs = soup.find_all("p")
        try:
            count = 0
            for i in forecasts:
                if i.find("img"):
                    forecast.append(self.clean_text(forecasts[count + 1].text.strip()))
                    forecast.append(self.clean_text(paragraphs[0].text.strip()))
                    break
                count += 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Fetch Validity forecast in table
        try:
            count = 0
            for i in forecasts:
                if "valid" in i.text.lower():
                    self.readings["valid"] = self.clean_text(
                        forecasts[count + 1].text.strip()
                    )
                    break
                count = count + 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Fetch Winds forecast in table
        try:
            count = 0
            for i in forecasts:
                if "Wind" in i.text:
                    if "m/s" in i.text:
                        self.readings["wind"] = (
                            self.clean_text(forecasts[count + 1].text.strip()) + " m/s"
                        )
                        break
                    else:
                        self.readings["wind"] = self.clean_text(
                            forecasts[count + 1].text.strip()
                        )
                        break
                count = count + 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Fetch Sunset forecast in table
        try:
            count = 0
            for i in forecasts:
                if "Sunset" in i.text:
                    self.readings["sunset"] = self.clean_text(
                        forecasts[count + 1].text.strip()
                    )
                    break
                count = count + 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Fetch Sunrise forecast in table
        try:
            count = 0
            for i in forecasts:
                if "Sunrise" in i.text:
                    self.readings["sunrise"] = self.clean_text(
                        forecasts[count + 1].text.strip()
                    )
                    break
                count = count + 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Fetch Temp forecast in table
        try:
            count = 0
            for i in forecasts:
                if "Temperature" in i.text:
                    self.readings["temperature"] = self.clean_text(
                        forecasts[count + 1].text.strip()
                    ).replace(u"\u2013", "-")
                    break
                count = count + 1
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Finished with 12Hr forecast table

        # Combine results in 12Hr forecast table to make over view
        try:
            self.readings["overview"] = "".join(forecast)
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Retrieve other information
        divs = soup.find_all(
            "div",
            {"class": "wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill"},
        )
        headers = []

        for div in divs:
            if div.find("h3"):
                headers.append(div)

        sections = headers[0].text.strip().split("\n")

        # Fetching Weather Forecast, Sea Conditions and Tide Information
        try:
            for section in sections:
                if "Tide" in section.strip():
                    tide = section.split(":", 1)
                    self.readings["tides"] = self.clean_text(tide[1])
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        try:
            for section in sections:
                if "Forecast" in section.strip():
                    tide = section.split(":", 1)
                    self.readings["forecast"] = self.clean_text(tide[1])
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        try:
            for section in sections:
                if "Sea Conditions" in section.strip():
                    tide = section.split(":", 1)
                    self.readings["seas"] = self.clean_text(tide[1])
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        try:
            for section in sections:
                if "WARNING" in section.strip():
                    tide = section.split(":", 1)
                    self.readings["warning"] = self.clean_text(tide[1])
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        try:
            for section in sections:
                if "EMERGENCY" in section.strip():
                    tide = section.split(":", 1)
                    self.readings["emergency"] = self.clean_text(tide[1])
        except Exception as e:
            print("Error while attempting to retrieve date {0}".format(e))

        # Specify the key of the data to be converted to voice
        self.readings["voiceField"] = "forecast"

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "emergency",
            "warning",
            "overview",
            "valid",
            "forecast",
            "seas",
            "wind",
            "tides",
            "sunrise",
            "sunset",
            "temperature",
            "date",
            "source",
            "order",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
    order = set(country.defineOrder())
    received_keys = set(country.readings.keys())
    print(order.difference(received_keys))
