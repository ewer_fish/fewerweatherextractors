import json

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "http://metproducts.gov.tt/weather/forecast_api2.php"
        )
        self.debug = False

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"waves": {"type": "numerical", "unit": "m"}}

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "synopsis"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        stuff = filter(None, soup.text.split("\n"))
        data = " ".join(stuff)

        # Extract the date and time
        try:
            spans = soup.find_all("span")
            issue_by = ""
            issue_when = ""
            issue_time = ""
            for sp in spans:
                span_content = sp.text.lower()
                if "issued" in span_content:
                    issue_time_tmp = self.clean_text(
                        ":".join(span_content.split(":")[1:])
                    )
                    if (
                        len(issue_time_tmp) > 2
                    ):  # due to the double span, we want to only get the full text
                        issue_time = issue_time_tmp
                elif "date:" in span_content:
                    issue_when_tmp = self.clean_text(span_content.split(":")[1])
                    if (
                        len(issue_when_tmp) > 2
                    ):  # due to the double span, we want to only get the full text
                        issue_when = issue_when_tmp
                elif "meteorologist" in span_content:
                    issue_by_tmp = self.clean_text(span_content.split(":")[1])
                    if (
                        len(issue_by_tmp) > 2
                    ):  # due to the double span, we want to only get the full text
                        issue_by = issue_by_tmp

            self.readings[
                "issued"
            ] = "Weather report was issued by {0} at {1} on {2}".format(
                issue_by, issue_time, issue_when
            )
            self.readings["date"] = "{0} at {1}".format(issue_when, issue_time)
        except Exception as e:
            print("Unable to retrieve issue data: {0}".format(e))
            self.send_error(
                "Failed to extract issue data", " Error report: {0}".format(e)
            )

        # Extract the waves
        try:
            # The data for the waves is inconsistent (the icon for the next section is either cloud or room)
            waves_tmp = data.lower().split("waves:")[1]
            # Try splitting via cloud icon (most common)
            try:
                waves_split = waves_tmp.split("cloud")
            except:
                waves_split = waves_tmp.split("room")

            if len(waves_split[1]) > 2:
                self.readings["waves"] = (
                    self.clean_text(waves_split[0])
                    .replace("\t", "")
                    .replace("in", " in")
                )
                # TODO need to use regular expression to match pattern X.Xmin to translate to X.Xm in
            else:
                raise Exception("Unable to split text for waves")
        except Exception as e:
            self.send_error(
                "Failed to extract wave data", " Error report: {0}".format(e)
            )

        # Extract the seas
        try:
            # so far the waves come after the seas, so we extract the data between
            self.readings["seas"] = self.clean_text(
                data.lower().split("seas:")[1].split("waves:")[0]
            )
        except Exception as e:
            self.send_error(
                "Failed to extract seas data", " Error report: {0}".format(e)
            )

        # Extracting the Sunset and Sunrise readings
        try:
            self.readings["sunrise"] = self.clean_text(
                data.lower().split("sunrise:")[1].split("sunset:")[0]
            )
            self.readings["sunset"] = self.clean_text(
                data.lower().split("sunset:")[1].split("info")[0]
            )
        except Exception as e:
            self.send_error(
                "Failed to extract sunset or sunrise data",
                " Error report: {0}".format(e),
            )

        # Extracting the synopsis from
        try:
            self.readings["synopsis"] = ""
            p_secs = soup.find_all("p")
            for secs in p_secs:
                if len(self.readings["synopsis"]) > 1:
                    self.readings["synopsis"] = self.readings["synopsis"] + " and, "
                self.readings["synopsis"] = self.readings["synopsis"] + self.clean_text(
                    secs.text
                )
        except Exception as e:
            self.send_error(
                "Failed to extract sunset or sunrise data",
                " Error report: {0}".format(e),
            )

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "synopsis",
            "issued",
            "seas",
            "waves",
            "Forecasted Max Temp, Piarco",
            "Forecasted Max Temp, Crown Point",
            "sunrise",
            "sunset",
            "order",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
