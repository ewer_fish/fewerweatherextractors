import json

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "https://www.barbadosweather.org/getMediaDataResp.php"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {"type": "text", "unit": "km/h"},
            "seas": {"type": "text", "unit": "m"},
        }

    def extract(self):
        self.readings = {
            "source": self.extractor_url,  # Setting the source for the readings
            "voiceField": "today synopsis",  # Specify the key of the data to be converted to voice
        }
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        # retrieve the weather forecast
        try:
            weather_forecast_div = soup.find("div", {"class": "mainPanelLeft"})
            wf_headings = weather_forecast_div.select("h5")
            wf_content = weather_forecast_div.select("p")

            # Can be made more general at a later time, however we will access directly for convenience
            first_forecast_name = self.clean_text(wf_headings[0].text).split(" ")[0].lower().strip()
            self.readings[first_forecast_name + " date"] = (
                self.clean_text(wf_headings[0].text).lower().strip().replace("from", "at")
            )
            first_forecast_synopsis = self.clean_text(wf_content[0].text).split(":")[1].strip()
            first_forecast_forecast = self.clean_text(wf_content[1].text).split(":")[1].strip()

            self.readings[first_forecast_name + " synopsis"] = first_forecast_synopsis
            self.readings[first_forecast_name + " forecast"] = first_forecast_forecast

            second_forecast_name = wf_headings[1].text.split(" ")[0].lower().strip()
            self.readings[second_forecast_name + " date"] = (
                wf_headings[1].text.lower().strip().replace("from", "at")
            )
            second_forecast_synopsis = self.clean_text(wf_content[2].text).split(":")[1].strip()
            second_forecast_forecast = self.clean_text(wf_content[3].text).split(":")[1].strip()

            self.readings[second_forecast_name + " synopsis"] = second_forecast_synopsis
            self.readings[second_forecast_name + " forecast"] = second_forecast_forecast
        except Exception as e:
            print("Failed to load weather forecasts: {0}".format(e))

        try:
            # retrieve wind forecasts
            wind_forecast_div = soup.find("div", {"class": "mainPanelRight"})
            wf_headings = wind_forecast_div.select("h5")
            wf_content = wind_forecast_div.select("p")

            print(wf_headings)

            first_wind_name = self.clean_text(wf_headings[0].text).split(" ")[0].lower().strip()
            self.readings[first_wind_name + " wind forecast"] = wf_content[
                0
            ].text.strip()

            second_wind_name = self.clean_text(wf_headings[1].text).split(" ")[0].lower().strip()
            self.readings[second_wind_name + " wind forecast"] = wf_content[
                1
            ].text.strip()
        except Exception as e:
            print("Failed to load wind forecasts: {0}".format(e))

        try:
            # retrieve marine forecasts
            marine_forecast_div = soup.find("div", {"class": "mainPanelRight2"})
            mf_headings = marine_forecast_div.select("h5")
            mf_content = marine_forecast_div.select("p")

            first_wind_name = mf_headings[0].text.split(" ")[0].lower().strip()
            self.readings[first_wind_name + " marine forecast"] = mf_content[
                0
            ].text.strip()

            second_wind_name = mf_headings[1].text.split(" ")[0].lower().strip()
            self.readings[second_wind_name + " marine forecast"] = mf_content[
                1
            ].text.strip()
        except Exception as e:
            print("Failed to load marine forecasts: {0}".format(e))

        try:
            main_double_panels = soup.find_all("div", {"class": "mainDoublePanel"})

            # Present Weather Fields
            present_weather_panels = main_double_panels[0]
            weather_rows = present_weather_panels.select("tr")
            for row in weather_rows:
                columns = row.select("td")
                weather_field = (
                    self.clean_text(columns[0].text).lower().replace("&degc;", "C")
                )
                self.readings[weather_field] = self.clean_text(columns[1].text)

            # Tides, Sunset and Sunrise
            tide_sun_panels = main_double_panels[3]
            tide_rows = tide_sun_panels.select("tr")
            for row in tide_rows:
                columns = row.select("td")
                tide_field = self.clean_text(columns[0].text).lower()
                self.readings[tide_field] = self.clean_text(columns[1].text)
        except Exception as e:
            print("Failed to load temperature or present weather: {0}".format(e))

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "today date",
            "today synopsis",
            "today forecast",
            "today seas",
            "today wind",
            "tonight date",
            "tonight synopsis",
            "tonight forecast",
            "tonight seas",
            "tonight wind",
            "tomorrow date",
            "tomorrow synopsis",
            "present wx",
            "air temp ( C )",
            "apparent temp ( C )",
            "relative humidity %",
            "wind speed (km/h)",
            "wind direction",
            "pressure (mb)",
            "visibility (km)",
            "sunset",
            "sunrise",
            "low tide",
            "high tide",
            "low tide (first)",
            "high tide (first)",
            "low tide (second)",
            "high tide (second)",
            "source",
            "order",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
    order = set(country.defineOrder())
    received_keys = set(country.readings.keys())
    print(order.difference(received_keys))
