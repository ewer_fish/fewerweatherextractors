import datetime
import json
import logging
from abc import ABCMeta, abstractmethod

import requests
from bs4 import BeautifulSoup

log = logging.getLogger(__name__)


# https://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide
# noinspection PyMethodMayBeStatic,SpellCheckingInspection
class WeatherSourceExtractor(object):
    __metaclass__ = ABCMeta
    # Not needed anymore but have to remove method from all extractors
    post_url = " "
    extractor_url = ""
    readings = {}
    http_headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36" # noqa
    }

    def __init__(self, extract=""):
        self.extractor_url = extract
        self.readings = {}

    @abstractmethod
    def get_extractor_url(self):
        pass

    @abstractmethod
    def get_reading_types(self):
        pass

    @abstractmethod
    def extract(self):
        pass

    @abstractmethod
    def defineOrder(self):
        pass

    @abstractmethod
    def toJSON(self):
        pass

    def save(self, sourceid=0, session=None, extract=True):
        data = self.extract()
        print("Attempting to save data: " + str(data))

    def clean_text(self, data):
        try:
            data = data.replace("\n", " ").replace("\r", " ")
        except Exception as e:
            log.error("Unable to replace break lines" + str(e))

        try:
            data = data.replace(u"\xa0", " ")
        except Exception as e:
            log.error("Unable to 1st ut8 character: " + str(e))

        try:
            data = data.replace(u"\xc2\xa0", " ")
        except Exception as e:
            log.error("Unable to 2nd ut8 character: " + str(e))

        try:
            data = data.replace(u"\u2026", " ")
        except Exception as e:
            log.error("Unable to 3rd ut8 character: " + str(e))

        try:
            data = data.replace(u"\xe2\x80\x89", " ")
        except Exception as e:
            log.error("Unable to 4th ut8 character: " + str(e))

        try:
            data = data.replace(u"\xb0", " ")
        except Exception as e:
            log.error("Unable to 5th ut8 character: " + str(e))

        try:
            data = data.replace(u"\u2019", " ")
        except Exception as e:
            log.error("Unable to 6th ut8 character: " + str(e))

        try:
            data = data.replace(u"\u2026", " ")
        except Exception as e:
            log.error("Unable to 7th ut8 character: " + str(e))

        try:
            data = data.replace(u"\u2013", "-")
        except Exception as e:
            log.error("Unable to 8th ut8 character: " + str(e))

        try:
            if "..." in data:
                data = data.replace("...", "")
        except Exception as e:
            log.error("Unable to 9th ut8 character: " + str(e))

        return data.strip()

    def findword(self, word, array):
        """
        Accepts array of HTML text content and returns which paragraph text is found in
        """
        items = []
        # for each element in the list of elements, check if the word is within the text
        for data in array:
            if (
                word in data.lower()
            ):  # If the word is in the elements, then add the element to the list
                items.append(data)
        return items  # Returns the list of instances

    def findwordList(self, word, array):
        save = ""
        for data in array:
            if word in data.lower():
                save = data
        return save

    def wordsize(self, stuff, word):
        string = ""
        pos = stuff.find(word) + len(word)
        while not (stuff[pos].isupper() and stuff[pos - 1].islower()):
            string += stuff[pos]
            pos += 1
        return string

    def comparison(self, newalert, oldalert):
        return set(newalert) == set(json.loads(oldalert))

    def weather_alert(self, sourceid):
        pass

    def send_error(
        self,
        title,
        description,
        err_type="error",
        filename="No File Specified",
        country=0,
    ):
        print("{0} - {1} - {2} - {3} - {4}".format(title, description, err_type, filename, country))

    def make_http_request_and_parse(self, request_type="get", use_soup=True, request_url=None):
        if request_url is None:
            request_url = self.extractor_url

        if request_type == "post":
            r = requests.post(request_url, headers=self.http_headers)
        else:
            r = requests.get(request_url, headers=self.http_headers)

        if use_soup:
            try:
                # use html5lib has it is more lenient against html errors
                soup = BeautifulSoup(r.content.decode("utf-8", "ignore"), "html5lib")
            except:
                # fallback to the built-in parser if unable to load html5lib
                soup = BeautifulSoup(r.content.decode("utf-8", "ignore"), "html.parser")
            return soup
        else:
            return r

    def default_date_reading(self):
        return datetime.datetime.now().strftime(
            "Weather readings were retrieved on %d, %b %Y at %r"
        )
