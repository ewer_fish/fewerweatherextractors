import json
import re

from bs4 import NavigableString

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "https://met.gov.lc/"
        )  # http://slumet.gov.lc/category/uncategorized/

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {"type": "numerical", "unit": "C"},
            "wind speed (kmh)": {"type": "numerical", "unit": "kmh"},
            "wind speed (mph)": {"type": "numerical", "unit": "mph"},
            "waves": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        items = []
        self.readings = {"source": self.extractor_url, "voiceField": "weather forecast"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        intro_data = soup.find_all("article")

        # get the first article
        article = intro_data[0]

        # get url to most recent forecast
        divs = article.findChild("div", recursive=False)
        inner_div = divs.findChild("div", recursive=False)
        forecast_link = inner_div.findChild("a", recursive=False)
        soup = self.make_http_request_and_parse(request_url=forecast_link["href"])

        # Capture all the paragraphs within the content section
        data = soup.find_all("div", {"class": "entry-content"})

        for row in data:
            for p_el in row.find_all("p"):
                if len(p_el.text) > 1:
                    items.append(self.clean_text(p_el.text))

        # Find all the paragraph tags
        p_data = data[0].find_all("p")
        self.readings["present weather"] = ""

        for paragraph in p_data:
            children_array = paragraph.contents
            for child in children_array:
                try:
                    if type(child) is NavigableString:
                        child_string = self.clean_text(child)
                        try:  # Extract Present Weather summary
                            if len(child_string.upper().split("PRESENT WEATHER")) > 1:
                                if "present weather" not in self.readings:
                                    self.readings["present weather"] = ""
                                self.readings["present weather"] += (
                                    child_string.lower().capitalize() + " "
                                )
                        except Exception as e:
                            print("Error parsing present weather" + str(e))

                        try:  # Extract Present Temperature and store as temperature
                            if (
                                len(child_string.upper().split("PRESENT TEMPERATURE"))
                                > 1
                            ):
                                if len(child_string.upper().split("C OR")) > 1:
                                    self.readings["temperature"] = (
                                        re.sub(
                                            "[^0-9]",
                                            "",
                                            child_string.upper().split("C OR")[0],
                                        )
                                        + " degrees celsius"
                                    )
                        except Exception as e:
                            print(e)

                        try:  # Extract Current Wind Measured at HEWANORRA
                            if len(child_string.upper().split("WIND AT HEWANORRA")) > 1:
                                self.readings[
                                    "current wind"
                                ] = child_string.lower().capitalize()
                        except Exception as e:
                            print(e)

                        try:  # Extract Forecast wind and use value for wind threshold-based measurement
                            if len(child_string.upper().split("WINDS WILL ")) > 1:
                                if len(child_string.upper().split("MPH OR")) > 1:
                                    self.readings["wind speed (mph)"] = re.sub(
                                        "[^0-9]",
                                        "",
                                        child_string.upper().split("MPH OR")[0],
                                    )

                                if len(child_string.upper().split("KM/H")) > 1:
                                    self.readings["wind speed (kmh)"] = re.sub(
                                        "[^0-9]",
                                        "",
                                        child_string.upper()
                                        .split("OR")[-1]
                                        .split("KM/H.")[0],
                                    )
                        except Exception as e:
                            print(e)

                        try:  # Extract Weather forecast
                            if len(child_string.upper().split("WEATHER:")) > 1:
                                self.readings["weather forecast"] = (
                                    child_string.upper()
                                    .split("WEATHER:")[1]
                                    .lower()
                                    .capitalize()
                                )
                        except Exception as e:
                            print(e)

                        try:  # Extract High and Low tides
                            if len(child_string.upper().split("TIDES FOR")) > 1:
                                split = child_string.lower().split(":")
                                if len(split) > 1:
                                    self.readings[self.clean_text(split[0])] = ":".join(
                                        split[1:]
                                    ).capitalize()
                        except Exception as e:
                            print(e)

                        try:  # Extract sea conditions and waves
                            if len(child_string.upper().split("SEAS:")) > 1:
                                split = child_string.lower().split(":")
                                if len(split) > 1:
                                    self.readings["seas"] = (
                                        split[1].lower().capitalize()
                                    )
                                    if len(split[1].upper().split("TO")) > 2:
                                        sec = split[1].upper().split("TO")[2]
                                        if len(sec.upper().split("FEET")) > 1:
                                            self.readings["waves"] = (
                                                re.sub(
                                                    "[^0-9]", "", sec.split("FEET")[0]
                                                )
                                                + " feet"
                                            )
                        except Exception as e:
                            print(e)

                        try:  # Extract Date
                            if len(child_string.upper().split("DATE:")) > 1:
                                self.readings["date"] = (
                                    child_string.upper()
                                    .split("DATE:")[1]
                                    .lower()
                                    .replace("\n", " ")
                                )
                        except Exception as e:
                            print("Error processing data: {0}".format(e))

                        try:  # Extract sunrise and sunset
                            sun = self.findword("sunset", items)[0].split(" ")
                            count = 0
                            for i in sun:
                                try:
                                    if "AM" in i.upper():
                                        self.readings["sunrise"] = (
                                            sun[count - 1].strip() + " AM"
                                        )
                                    elif "PM" in i.upper():
                                        self.readings["sunset"] = (
                                            sun[count - 1].strip() + " PM"
                                        )
                                    count += 1
                                except Exception as e:
                                    print(
                                        "Error processing sunset and sunrise: {0}".format(
                                            e
                                        )
                                    )
                        except Exception as e:
                            print("Error processing sunset and sunrise: {0}".format(e))

                        try:  # Extract temperature
                            if "temperature" not in self.readings:
                                temperature = self.findword("temperature", items)[
                                    0
                                ].split("C OR")[0]
                                self.readings["temperature"] = (
                                    self.clean_text(re.sub("[^0-9]", "", temperature))
                                    + " C"
                                )
                        except Exception as e:
                            print("Unable to temperautre for st lucia" + str(e))

                        try:  # Extract current wind
                            if "current wind" not in self.readings:
                                p_el = (
                                    self.findwordList("wind", items).lower().split("\n")
                                )
                                for i in range(len(p_el)):
                                    if "wind" in p_el[i]:
                                        self.readings["current wind"] = p_el[
                                            i
                                        ].capitalize()
                        except Exception as e:
                            print("Unable to retrieve current wind st lucia" + str(e))

                except Exception as e:
                    print("Error occurred" + str(e))

        for key in self.readings.keys():
            self.readings[key] = self.clean_text(self.readings[key])
        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "date",
            "present weather",
            "weather forecast",
            "temperature",
            "current wind",
            "wind speed (kmh)",
            "wind speed (mph)",
            "tides for castries harbour",
            "tides for vieux fort bay",
            "seas",
            "waves",
            "sunrise",
            "sunset",
            "source",
            "order",
        ]


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
