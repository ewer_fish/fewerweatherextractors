import json

from bs4 import NavigableString

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        # #2. Add source URL for MET website in file
        WeatherSourceExtractor.__init__(self, "url")
        ##

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "temperature": {"type": "numerical", "unit": "C"},
            "wind speed (kmh)": {"type": "numerical", "unit": "kmh"},
            "wind speed (mph)": {"type": "numerical", "unit": "mph"},
            "waves": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        items = []
        # forecast = []
        self.readings = {}
        data = []
        # #3. Add code to get latest data
        # # Make Request from the Website
        # r = requests.get(self.extractor_url)
        # soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")

        # introData = soup.find_all("article")

        # #get the first article
        # article = introData[0]

        # #get url to most recent forecast
        # divs = article.findChild("div", recursive=False)
        # innerDiv = divs.findChild("div", recursive=False)
        # forecastLink = innerDiv.findChild("a", recursive=False)

        # r = requests.get(forecastLink['href'])
        # soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")

        # # Capture all the paragraphs within the content section
        # data = soup.find_all("div", {"class": "entry-content"})

        # #print(data)#debug print to screen to show latest data captured

        ##

        for row in data:
            for list in row.find_all("p"):
                if len(list.text) > 1:
                    # print(list.text)#debug print to screen to show latest data captured
                    items.append(self.clean_text(list.text))

        # Find all the paragraph tags
        p_data = data[0].find_all("p")

        for paragraph in p_data:
            children_array = paragraph.contents
            for child in children_array:
                try:
                    if type(child) is NavigableString:
                        childString = self.clean_text(child) # noqa
                        # 4. Add code to parse latest data for "Present Weather"
                        # try:  # Extract Present Weather summary
                        #     if len(childString.upper().split("PRESENT WEATHER")) > 1:
                        #         if 'present weather' not in self.readings:
                        #             self.readings['present weather'] = ""
                        #         self.readings['present weather'] += childString.lower().capitalize() + " "
                        # except Exception as e:
                        #     print 'Error parsing present weather' + str(e)

                        # 5. Add code to parse latest data for "Present Temperature"
                        # try:  # Extract Present Temperature and store as temperature
                        #     if len(childString.upper().split("PRESENT TEMPERATURE")) > 1:
                        #         if len(childString.upper().split("C OR")) > 1:
                        #             self.readings['temperature'] = re.sub("[^0-9]", "",
                        #             childString.upper().split("C OR")[0]) + ' degrees celsius'
                        # except Exception as e:
                        #     print(e)

                        # try:  # Extract temperature
                        #     if 'temperature' not in self.readings:
                        #         temperature = self.findword('temperature', items)[0].split("C OR")[0]
                        #         self.readings['temperature'] = self.clean_text(re.sub("[^0-9]", "", temperature)) + " C" # noqa
                        # except Exception as e:
                        #     print("Unable to temperautre for st lucia" + str(e))

                except Exception as e:
                    print("Error occurred" + str(e))
        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "date",
            "present weather",
            "weather forecast",
            "temperature",
            "current wind",
            "wind speed (kmh)",
            "wind speed (mph)",
            "tides for castries harbour",
            "tides for vieux fort bay",
            "seas",
            "waves",
            "sunrise",
            "sunset",
            "source",
            "order",
        ]


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
