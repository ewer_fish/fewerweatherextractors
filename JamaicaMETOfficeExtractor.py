import datetime
import json

try:
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "http://jamaica.weatherproof.fi/web/website_2014/website_elements/news2.php?type=8",
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {"type": "numerical", "unit": "mph"},
            "seas": {"type": "numerical", "unit": "feet"},
        }

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "comment"}
        soup = self.make_http_request_and_parse()
        self.readings["date"] = self.default_date_reading()

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, 'w') as fp:
        #     fp.write(str(soup))
        # with open(filename, "r") as fp:
        #     content = fp.read()
        # soup = BeautifulSoup(content, "html5lib")

        p_el = soup.find("p", {"class": "release"})
        self.readings["date"] = self.clean_text(p_el.text)

        try:
            p_el = soup.find("p", {"class": "highlight"})
            p_el = p_el.find_next("b")
            sig_feature_text = p_el.next_sibling
            self.readings["Significant Feature"] = self.clean_text(sig_feature_text)
        except Exception as e:
            print("Error retrieving significant feature {0}".format(e))

        try:
            p_el = soup.find("div", {"class": "body"})
            p_el = p_el.find_next("b")
            if "comment" in p_el.text.lower():
                comment_text = p_el.next_sibling
                self.readings["comment"] = self.clean_text(comment_text)
        except Exception as e:
            print("Error retrieving comment {0}".format(e))

        p_el = p_el.find_next("b")
        while p_el is not None:
            p_el = p_el.find_next("b")
            if p_el is not None:
                if "morning" in p_el.text.lower():
                    try:
                        if "this morning" in p_el.text.lower():
                            temp_el = p_el
                            morning_text = temp_el.next_sibling
                            self.readings["this morning"] = self.clean_text(
                                morning_text
                            )
                        elif "tomorrow morning" in p_el.text.lower():
                            temp_el = p_el
                            morning_text = temp_el.next_sibling
                            self.readings["tomorrow morning"] = self.clean_text(
                                morning_text
                            )
                    except Exception as e:
                        print("Error retrieving morning {0}".format(e))
                elif "afternoon" in p_el.text.lower():
                    try:
                        if "this afternoon" in p_el.text.lower():
                            temp_el = p_el
                            afternoon_text = temp_el.next_sibling
                            self.readings["this afternoon"] = self.clean_text(
                                afternoon_text
                            )
                        elif "tomorrow afternoon" in p_el.text.lower():
                            temp_el = p_el
                            afternoon_text = temp_el.next_sibling
                            self.readings["tomorrow afternoon"] = self.clean_text(
                                afternoon_text
                            )
                    except Exception as e:
                        print("Error retrieving afternoon {0}".format(e))
                elif "temperature" in p_el.text.lower():
                    if "kingston" in p_el.text.lower():
                        try:
                            if "maximum" in p_el.text.lower():
                                temp_el = p_el
                                tonight_text = temp_el.next_sibling
                                self.readings[
                                    "maximum temperature kingston"
                                ] = self.clean_text(tonight_text)
                            if "minimum" in p_el.text.lower():
                                temp_el = p_el
                                tonight_text = temp_el.next_sibling
                                self.readings[
                                    "minimum temperature kingston"
                                ] = self.clean_text(tonight_text)
                        except Exception as e:
                            print("Error retrieving temperature {0}".format(e))
                    elif "montego bay" in p_el.text.lower():
                        try:
                            if "maximum" in p_el.text.lower():
                                temp_el = p_el
                                tonight_text = temp_el.next_sibling
                                self.readings[
                                    "maximum temperature montego bay"
                                ] = self.clean_text(tonight_text)
                            if "minimum" in p_el.text.lower():
                                temp_el = p_el
                                tonight_text = temp_el.next_sibling
                                self.readings[
                                    "minimum temperature montego"
                                ] = self.clean_text(tonight_text)
                        except Exception as e:
                            print(
                                "Error retrieving montego bay temperature {0}".format(e)
                            )
                elif "tonight" in p_el.text.lower():
                    try:
                        temp_el = p_el
                        tonight_text = temp_el.next_sibling
                        self.readings["tonight"] = self.clean_text(tonight_text)
                    except Exception as e:
                        print("Error retrieving tonight {0}".format(e))

        # Set the default date for currently
        self.readings["date_retrieved"] = datetime.datetime.now().strftime(
            "Weather readings were retrieved on %d, %b %Y at %r"
        )
        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "date",
            "comment",
            "Significant Feature",
            "this morning",
            "this afternoon",
            "tomorrow morning",
            "tomorrow afternoon",
            "tonight",
            "max temp kingston",
            "max temp montego bay",
            "min temp Kingston",
            "min temp Montego Bay",
            "three day forecast",
            "source",
            "order",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
