import json

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


# St.VincentCurrentForecastExtractor
class Extractor(WeatherSourceExtractor):
    def __init__(self):
        self.display_url = "http://nms.gov.bz/forecast/general-weather-forecast/"
        WeatherSourceExtractor.__init__(
            self, "http://db.hydromet.gov.bz:8181/api/forecast_marine/read.php"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"wind": {"type": "text", "unit": "kts"}}

    def extract(self):
        self.readings = {"source": self.extractor_url, "voiceField": "forecast"}
        res = self.make_http_request_and_parse(request_type="post", use_soup=False)
        data = res.json()
        expected_fields = [
            "marine_forecast_type",
            "synopsis",
            "advisory",
            "sea_surface_temperature",
            "max_temperature",
            "min_temperature",
            "outlook",
        ]
        expected_marine_fields = [
            "wind_speed",
            "wind_direction",
            "wind_condition",
            "sea_state",
            "waves",
            "info",
        ]
        forecast_data = data["forecast_data"][0]
        self.readings["forecaster"] = "{0} {1}".format(
            forecast_data["first_name"], forecast_data["last_name"]
        )
        self.readings["date"] = "{0} at {1}".format(
            forecast_data["forecast_date"], forecast_data["forecast_time"]
        )
        self.readings["created_time"] = forecast_data["created_time"]
        for field in expected_fields:
            title = field
            if len(field.split("_")) > 1:
                title = " ".join(title.split("_"))

            if len(forecast_data[field]) > 0:
                self.readings[title] = forecast_data[field]
                if "temperature" in field:
                    self.readings[title] += " F"

        marine_details = forecast_data["marine_details"]
        for marine_detail in marine_details[:2]:
            day_period = marine_detail["marine_type"]
            for field in expected_marine_fields:
                title = field
                if len(field.split("_")) > 1:
                    title = " ".join(title.split("_"))
                key = "{0} Marine {1}".format(day_period, title)
                self.readings[key] = marine_detail[field]
                if "waves" in field:
                    self.readings[key] += " ft"
                if "wind_speed" in field and len(self.readings[key]) > 1:
                    self.readings[key] += " kts"

        tide_details = forecast_data["tide_details"]
        for tide_detail in tide_details:
            key = "{0} {1} Tide".format(
                tide_detail["tide_date_type"], tide_detail["tide_type"]
            )
            reading = "{0} at {1}".format(
                tide_detail["tide_time"], tide_detail["tide_display"]
            )
            self.readings[key] = reading

        sun_details = forecast_data["sun_details"]
        for sun_detail in sun_details:
            key = "{0} {1} Tide".format(
                sun_detail["sun_date_type"], sun_detail["sun_type"]
            )
            reading = "{0} at {1}".format(
                sun_detail["sun_time"], sun_detail["sun_display"]
            )
            self.readings[key] = reading

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def defineOrder(self):
        return [
            "advisory",
            "forecast",
            "outlook",
            "winds",
            "today wind condition",
            "tonight wind condition",
            "tomorrow wind condition",
            "today wind speed",
            "tonight wind speed",
            "tomorrow wind speed",
            "today wind direction",
            "tonight wind direction",
            "tomorrow wind direction",
            "sea state",
            "today sea state",
            "tonight sea state",
            "tomorrow sea state",
            "temperature",
            "max temperature" "high tide",
            "low tide",
            "today high tide",
            "today low tide",
            "tomorrow high tide",
            "tomorrow low tide",
            "sunrise",
            "sunset",
            "forecaster",
            "date",
            "source",
        ]


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
    order = set(country.defineOrder())
    received_keys = set(country.readings.keys())
    print(order.difference(received_keys))
