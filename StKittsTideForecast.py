import json
import time
from datetime import datetime, date

import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import (
        WeatherSourceExtractor,
    )
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self, "https://www.tide-forecast.com/locations/Basseterre/tides/latest"
        )

        self.timezone = "AST"

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {"tides": {"type": "text", "unit": ""}}

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    @staticmethod
    def _metric_to_feet(level_in_metric):
        return round(float(level_in_metric) * 3.28084, 1)

    @staticmethod
    def _tide_date_to_string(date_obj):
        return time.mktime(date_obj.timetuple())

    @staticmethod
    def _build_tide_date(tide_date, tide_year, tide_time=None):
        if tide_time:
            date_time_str = "{0} {1} {2}".format(tide_date, tide_year, tide_time)
            return datetime.strptime(date_time_str, "%d %b %Y %H:%M%p")
        else:
            date_time_str = "{0} {1}".format(tide_date, tide_year)
            return datetime.strptime(date_time_str, "%d %b %Y")

    @staticmethod
    def _sort_tide_records(tide_records):
        return sorted(tide_records, key=lambda tide: (tide["timestamp"], tide["event"]))

    def _generate_timestamp_for_tide_record(self, tide_date, tide_time):
        tide_year = date.today().year
        try:
            tide_date_obj = self._build_tide_date(tide_date, tide_year, tide_time)
            return self._tide_date_to_string(tide_date_obj)
        except Exception as e:
            print(
                "error occurred when attempting to parse date with time. Attempting to parse simpler date:",
                e,
            )
            try:
                tide_date_obj = self._build_tide_date(tide_date, tide_year)
                return self._tide_date_to_string(tide_date_obj)
            except Exception as ex:
                print("error occurred when attempting to parse date without time", ex)

        return None

    def _get_tide_column_values(
        self, tide_columns, tide_span_name, date_records, tide_type
    ):
        assert len(tide_columns) == len(date_records)

        records = []
        for idx, td_el in enumerate(tide_columns):
            tide_date = date_records[idx]

            tide_record = {
                "date": tide_date,
                "time_zone": self.timezone,
                "event": tide_type,
            }

            if td_el.find("div", {"class": "tide-table__tide-time-filler"}):
                tide_record.update(
                    {
                        "time": "N/A",
                        "unit": "N/A",
                        "level_metric": "N/A",
                        "level_imperial": "N/A",
                        "timestamp": "N/A",
                    }
                )
            else:
                tide_time = td_el.find("span", {"class": tide_span_name}).text.strip()
                height = td_el.find("span", {"class": "heighttide"}).text
                height_unit = td_el.find("span", {"class": "heightu"}).text
                tide_record.update(
                    {
                        "time": tide_time,
                        "unit": height_unit,
                        "level_metric": float(height),
                        "level_imperial": self._metric_to_feet(height),
                        "timestamp": self._generate_timestamp_for_tide_record(
                            tide_date, tide_time
                        ),
                    }
                )

            records.append(tide_record)

        return records

    def extract(self):
        self.readings = {}
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content.decode("utf-8", "ignore"), "html.parser")

        tide_table = soup.find("table", {"class": "tide-table__table"})
        tide_rows = tide_table.select("tr")

        dates_row = tide_rows[0]
        date_records = []
        dates_columns = dates_row.select("th")
        for td_el in dates_columns:
            tide_date = td_el.text
            date_records.append(tide_date)

        day_records = []
        day_row = tide_rows[1]
        day_columns = day_row.select("td")
        for td_el in day_columns:
            day = td_el.text
            day_records.append(day)

        high_tide_row = tide_rows[3]
        high_tide_columns = high_tide_row.select("td")

        low_tide_row = tide_rows[4]
        low_tide_columns = low_tide_row.select("td")

        low_tide_span = "tide-table__value-low"
        high_tide_span = "tide-table__value-high"

        high_tide_records = self._get_tide_column_values(
            tide_columns=high_tide_columns,
            tide_span_name=high_tide_span,
            date_records=date_records,
            tide_type="High Tide",
        )

        low_tide_records = self._get_tide_column_values(
            tide_columns=low_tide_columns,
            tide_span_name=low_tide_span,
            date_records=date_records,
            tide_type="Low Tide",
        )

        self.readings["tides"] = self._sort_tide_records(
            high_tide_records + low_tide_records
        )

        return self.readings

    def defineOrder(self):
        order = []
        return order


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    print(country.toJSON())
    with open("output.json", "w") as fp:
        fp.write(country.toJSON())
        fp.close()
