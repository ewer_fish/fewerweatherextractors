import logging
import os
import sys
import unittest
from os.path import realpath

import requests

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

extractor_path_from_execution = "."
extractor_files = os.listdir(extractor_path_from_execution)
if len(extractor_files) < 5:
    extractor_path_from_execution = "../"

sys.path.append(realpath(extractor_path_from_execution))


class TestExtractors(unittest.TestCase):
    def setUp(self):
        # A hack to attempt to get the folder with all the extractors
        packages = self.get_all_package_names(
            extractor_folder="./", base_path=extractor_path_from_execution
        )
        self.packages = self.filter_extractor_packages(packages)
        log.info("Size of packages: {0}".format(len(list(self.packages))))

    @staticmethod
    def get_all_package_names(extractor_folder="extractors", base_path=None):
        #  this is a list of Extractors available
        packages = []
        if not base_path:
            base_path = os.path.dirname(os.path.realpath(__file__))
        extractor_path = realpath("{0}/{1}/".format(base_path, extractor_folder))

        for i in os.listdir(extractor_path):
            if len(i.split(".")) == 2:
                if i.split(".")[1] == "py":
                    packages.append(i.split(".")[0])
        return packages

    @staticmethod
    def filter_extractor_packages(packages):
        return filter(
            lambda package: (
                "WorldTides" not in package
                and "__init__" not in package
                and "TestMETOfficeExtractor" not in package
                and "WeatherSourceExtractor" not in package
            ),
            packages,
        )

    @staticmethod
    def initiate_extractor(package):
        module = __import__(package)
        weather_class = getattr(module, "Extractor")
        return weather_class()

    def test_all_extractors_are_instance_of_weather_extractor(self):
        extractor_parent_module = __import__("WeatherSourceExtractor")
        extractor_parent_class = getattr(
            extractor_parent_module, "WeatherSourceExtractor"
        )
        for package in self.packages:
            log.info("Is {0} an instance of WeatherSourceExtractor?".format(package))
            extractor_instance = self.initiate_extractor(package)
            self.assertTrue(
                isinstance(extractor_instance, extractor_parent_class),
                "Extractor {0} was not setup properly".format(package),
            )

    def test_all_extractors_have_valid_urls_via_property(self):
        for package in self.packages:
            log.info("does {0} have a valid url?".format(package))
            extractor_instance = self.initiate_extractor(package)
            self.assertTrue(
                len(extractor_instance.extractor_url) > 0,
                "The URL for extractor {0} was not properly configured".format(package),
            )

    def test_all_extractors_have_valid_urls_via_request(self):
        for package in self.packages:
            extractor_instance = self.initiate_extractor(package)
            if "GrenadaMETOfficeExtractor" not in package:
                headers = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) "
                    "AppleWebKit/537.36 (KHTML, like Gecko) "
                    "Chrome/85.0.4183.83 Safari/537.36"
                }
                res = requests.head(
                    url=extractor_instance.extractor_url, headers=headers
                )
                self.assertEqual(
                    res.status_code,
                    200,
                    "The URL for extractor {0} was not properly configured".format(
                        package
                    ),
                )

    def test_all_extractors_have_valid_urls_via_accessor_method(self):
        for package in self.packages:
            log.info("does {0} have a valid url?".format(package))
            extractor_instance = self.initiate_extractor(package)
            self.assertTrue(
                len(extractor_instance.get_extractor_url()) > 0,
                "The URL for extractor {0} was not properly configured".format(package),
            )

    def test_all_extractors_have_valid_reading_types(self):
        for package in self.packages:
            log.info("does {0} have a valid reading_types?".format(package))
            extractor_instance = self.initiate_extractor(package)
            self.assertTrue(
                len(extractor_instance.get_reading_types().keys()) > 0,
                "The reading_types for extractor {0} was not configured".format(
                    package
                ),
            )

    def test_all_extractors_have_a_defined_order(self):
        for package in self.packages:
            log.info("does {0} have a valid defined_order?".format(package))
            extractor_instance = self.initiate_extractor(package)
            self.assertIsNotNone(
                extractor_instance.defineOrder(),
                "The defined_order for extractor {0} was not configured".format(
                    package
                ),
            )
            if "tide" not in package.lower():
                self.assertTrue(
                    len(extractor_instance.defineOrder()) > 0,
                    "The defined_order for extractor {0} was not configured".format(
                        package
                    ),
                )

    def _country_met_extractor_helper(
        self, country="stvincent", hard_order_requirement=False
    ):
        country_packages = filter(
            lambda package: country in package.lower() and "met" in package.lower(),
            self.packages,
        )
        for package in country_packages:
            print(
                "Attempting to test package {0} for country {1}".format(
                    package, country
                )
            )
            extractor = self.initiate_extractor(package)
            res = extractor.extract()
            self.assertIsNotNone(res, "No results: {0}".format(package))
            self.assertGreater(
                len(res.keys()),
                4,
                "Keys for {0} was les than expected: {1}".format(country, res.keys()),
            )
            field_order = extractor.defineOrder()
            for rec in res:
                if rec in field_order:
                    self.assertGreater(
                        len(res[rec]), 0, "{0} was not in properly defined".format(rec)
                    )
            for field in field_order:
                is_field_from_order_in_results = field in res
                # log.info("Check if order field {0} in results was {1}".format(field, is_field_from_order_in_results))
                if hard_order_requirement:
                    self.assertTrue(is_field_from_order_in_results)
                else:
                    if not is_field_from_order_in_results:
                        log.warn("Field {0} not in {1}".format(field, res))

    def test_all_met_extractors_can_extract_data_without_error(self):
        countries = [
            "anguilla",
            "antigua",
            "barbados",
            "belize",
            "bvi",
            "dominica",
            # "grenada",
            "guyana",
            "jamaica",
            "stvincent",
            "trinidad",
        ]
        for country in countries:
            print("Attempting to run extractor for {0}".format(country))
            self._country_met_extractor_helper(country=country)
        # self._country_met_extractor_helper(country="belize")
